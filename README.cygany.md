This is the debian-archive-keyring package from the salsa.debian.org, with our minor modifications
to become compilable under cygany, which a debian overlay over cygwin.

Branch tracks master in the salsa repository, roughly so:

Git remotes:
```
origin	 git@gitlab.com:cygany/debian-archive-keyring.git (fetch)
origin	 git@gitlab.com:cygany/debian-archive-keyring.git (push)
upstream https://salsa.debian.org/release-team/debian-archive-keyring.git (fetch)
upstream https://salsa.debian.org/release-team/debian-archive-keyring.git (push)
```

Branch setup:
```
* cygany               7ced903 [origin/cygany] extended gitignore, gpg to gpg2 because cygwin still did not killed gpg1
  master               ba826f2 [upstream/master] Release debian-archive-keyring/2023.4
```

In our current state, it can be only compiled roughly so:

```
PERL5LIB=/usr/share/perl5 DEB_RULES_REQUIRES_ROOT=no debian/rules build binary
```

But soon it will be hopefully better.
